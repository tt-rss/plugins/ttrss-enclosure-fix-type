Overrides content type for enclosures based on data returned by HTTP server
for enclosure URLs. Helps if enclosures are not detected properly as images, etc.

Enable for specific feeds in the feed editor.

Git clone to ``(tt-rss-root)/plugins.local/af_enclosure_fix_type``.
