<?php

class Af_Enclosure_Fix_Type extends Plugin {

	/** @var PluginHost $host */
	private $host;

	function about() {
		return [null,
			"Overrides content type for enclosures based on server-provided data",
			"fox"];
	}

	function init($host) {
		$this->host = $host;

		$this->host->add_hook($host::HOOK_ENCLOSURE_IMPORTED, $this);
		$this->host->add_hook($host::HOOK_PREFS_EDIT_FEED, $this);
		$this->host->add_hook($host::HOOK_PREFS_SAVE_FEED, $this);

		//$host->add_filter_action($this, "action_fix_enclosure_type", __("Fix media enclosures"));
	}

	function hook_prefs_edit_feed($feed_id) {
		$enabled_feeds = $this->host->get_array($this, "enabled_feeds");
		?>
		<header><?= $this->__("Override enclosure content type") ?></header>
		<section>
			<fieldset>
				<label class='checkbox'>
					<?= \Controls\checkbox_tag("af_enclosure_fix_type_enabled", in_array($feed_id, $enabled_feeds)) ?>
					<?= $this->__('Enable for this feed') ?></label>
			</fieldset>
		</section>
		<?php
	}

	function hook_prefs_save_feed($feed_id) {
		$enabled_feeds = $this->filter_unknown_feeds(
			$this->host->get_array($this, "enabled_feeds"));

		$enable = checkbox_to_sql_bool($_POST["af_enclosure_fix_type_enabled"] ?? "");
		$key = array_search($feed_id, $enabled_feeds);

		if ($enable) {
			if ($key === false) {
				array_push($enabled_feeds, $feed_id);
			}
		} else {
			if ($key !== false) {
				unset($enabled_feeds[$key]);
			}
		}

		$this->host->set($this, "enabled_feeds", $enabled_feeds);
	}

	function hook_enclosure_imported($enc, $feed) {
		$enabled_feeds = $this->host->get($this, "enabled_feeds");

		if (!is_array($enabled_feeds) || array_search($feed, $enabled_feeds) === FALSE)
			return $enc;

		// TODO: php7 compatibility needs to be officially dropped at some point

		// @phpstan-ignore-next-line
		$headers = get_headers($enc->link, 1);

		if (is_array($headers) && isset($headers["Content-Type"])) {
			$enc->type = $headers["Content-Type"];
		}

		return $enc;
	}

	/**
	 * @param array<int> $enabled_feeds
	 * @return array<int>
	 * @throws PDOException
	 */
	private function filter_unknown_feeds(array $enabled_feeds) : array {
		$tmp = array();

		foreach ($enabled_feeds as $feed) {

			$sth = $this->pdo->prepare("SELECT id FROM ttrss_feeds WHERE id = ? AND owner_uid = ?");
			$sth->execute([$feed, $_SESSION['uid']]);

			if ($row = $sth->fetch()) {
				array_push($tmp, $feed);
			}
		}

		return $tmp;
	}

	function api_version() {
		return 2;
	}
}
